package ru.buzanov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;
import ru.buzanov.tm.entity.Project;

import javax.persistence.Cacheable;
import javax.persistence.QueryHint;
import java.util.Collection;

@Repository

public interface ProjectRepository extends JpaRepository<Project, String> {
//    @QueryHints(@QueryHint(name = org.hibernate.annotations.QueryHints.CACHEABLE, value = "true"))
    boolean existsByUserIdAndName(String userId, String name);

    Project findByUserIdAndId(String userId, String id);

    Collection<Project> findAllByUserId(String userId);

    Collection<Project> findAllByUserIdAndNameContaining(String userId, String name);

    Collection<Project> findAllByUserIdAndDescriptionContaining(String userId, String description);

    void deleteByUserIdAndId(String userId, String id);

    void deleteAllByUserId(String userId);

    Collection<Project> findAllByUserIdOrderByDescriptionAsc(String userId);

    Collection<Project> findAllByUserIdOrderByNameAsc(String userId);

    Collection<Project> findAllByUserIdOrderByStartDateAsc(String userId);

    Collection<Project> findAllByUserIdOrderByFinishDateAsc(String userId);
}