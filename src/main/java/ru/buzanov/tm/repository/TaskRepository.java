package ru.buzanov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;
import ru.buzanov.tm.entity.Task;

import javax.persistence.Cacheable;
import javax.persistence.QueryHint;
import java.util.Collection;

@Repository
//@Cacheable
public interface TaskRepository extends JpaRepository<Task, String> {
//    @QueryHints(@QueryHint(name = org.hibernate.annotations.QueryHints.CACHEABLE, value = "true"))

    boolean existsByUserIdAndName(String userId, String name);

    Task findByUserIdAndId(String userId, String id);

    Collection<Task> findAllByUserId(String userId);

    Collection<Task> findAllByUserIdAndProjectId(String userId, String projectId);

    Collection<Task> findAllByUserIdAndNameContaining(String userId, String name);

    Collection<Task> findAllByUserIdAndDescriptionContaining(String userId, String description);

    void deleteByUserIdAndId(String userId, String id);

    void deleteAllByUserId(String userId);

    void deleteAllByUserIdAndProjectId(String userId, String projectId);

    Collection<Task> findAllByUserIdOrderByDescriptionAsc(String userId);

    Collection<Task> findAllByUserIdOrderByNameAsc(String userId);

    Collection<Task> findAllByUserIdOrderByStartDateAsc(String userId);

    Collection<Task> findAllByUserIdOrderByFinishDateAsc(String userId);
}