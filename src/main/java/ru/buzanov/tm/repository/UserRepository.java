package ru.buzanov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;
import ru.buzanov.tm.entity.User;

import javax.persistence.Cacheable;
import javax.persistence.QueryHint;

@Repository
//@Cacheable
public interface UserRepository extends JpaRepository<User, String> {
//    @QueryHints(@QueryHint(name = org.hibernate.annotations.QueryHints.CACHEABLE, value = "true"))

    boolean existsByLogin(String login);

    User findByLogin(String login);

}