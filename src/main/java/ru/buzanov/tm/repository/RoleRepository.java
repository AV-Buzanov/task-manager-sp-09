package ru.buzanov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;
import ru.buzanov.tm.entity.Roles;
import ru.buzanov.tm.enumerated.RoleType;

import javax.persistence.Cacheable;
import javax.persistence.QueryHint;
import java.util.Collection;

@Repository
//@Cacheable
public interface RoleRepository extends JpaRepository<Roles, String> {
//    @QueryHints(@QueryHint(name = org.hibernate.annotations.QueryHints.CACHEABLE, value = "true"))

    Collection<Roles> findAllByUserId(String userId);
    Collection<Roles> findAllByRoleType(RoleType roleType);

}
