package ru.buzanov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.springframework.cache.annotation.Cacheable;


import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@org.springframework.cache.annotation.Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@MappedSuperclass
public abstract class AbstractEntity implements Serializable {
    public AbstractEntity(@NotNull String id) {
        this.id = id;
    }
    @Id
    @NotNull private String id = UUID.randomUUID().toString();
    private static final long serialVersionUID = 1L;
}
