package ru.buzanov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.buzanov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@MappedSuperclass
public abstract class AbstractWBS extends AbstractEntity {
    public AbstractWBS(@NotNull String id) {
        super(id);
    }

    @Nullable
    private String name;
    @ManyToOne
    @JoinColumn(name="user_id")
    @Nullable
    private User user;
    @Enumerated(value = EnumType.STRING)
    @NotNull
    private Status status = Status.PLANNED;
    @DateTimeFormat(pattern = "MM-dd-yyyy")
    @NotNull
    private Date createDate = new Date(System.currentTimeMillis());
    @Nullable
    @DateTimeFormat(pattern = "MM-dd-yyyy")
    private Date startDate;
    @Nullable
    @DateTimeFormat(pattern = "MM-dd-yyyy")
    private Date finishDate;
    @Nullable
    private String description;
}