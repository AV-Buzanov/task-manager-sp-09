package ru.buzanov.tm.controller.face;

import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.primefaces.PrimeFaces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;
import ru.buzanov.tm.dto.UserDTO;
import ru.buzanov.tm.enumerated.RoleType;
import ru.buzanov.tm.service.UserService;

import javax.faces.application.FacesMessage;

@Component("reg")

@SessionScope
@ELBeanName(value = "reg")
@Join(path = "/reg", to = "/faces/register.xhtml")
public class Reg {
}
