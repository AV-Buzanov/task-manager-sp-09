package ru.buzanov.tm.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.buzanov.tm.dto.TaskDTO;
import ru.buzanov.tm.security.CustomUser;
import ru.buzanov.tm.service.TaskService;

import java.util.Collection;

@RestController
@PreAuthorize("isAuthenticated()")
@RequestMapping(value = "/rest/task")
public class TaskRestController {
    @Autowired
    private TaskService taskService;

    @RequestMapping(value = "/findAll", method = RequestMethod.GET)
    public ResponseEntity<Collection<TaskDTO>> listTask(@AuthenticationPrincipal CustomUser user) throws Exception {
        return ResponseEntity.ok(taskService.findAll(user.getUserId()));
    }

    @RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
    public ResponseEntity<TaskDTO> getTask(@AuthenticationPrincipal CustomUser user, @PathVariable final String id) throws Exception {
        return ResponseEntity.ok(taskService.findOne(user.getUserId(), id));
    }

    @RequestMapping(value = "/mergeAll", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<TaskDTO>> mergeAllTask(@AuthenticationPrincipal CustomUser user, @RequestBody Collection<TaskDTO> tasks) throws Exception {
        for (TaskDTO task : tasks)
            taskService.merge(user.getUserId(), task.getId(), task);
        return ResponseEntity.ok(taskService.findAll(user.getUserId()));
    }

    @RequestMapping(value = "/merge", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TaskDTO> mergeTask(@AuthenticationPrincipal CustomUser user, @RequestBody TaskDTO task) throws Exception {
        taskService.merge(user.getUserId(), task.getId(), task);
        return ResponseEntity.ok(taskService.findOne(user.getUserId(), task.getId()));
    }

    @RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
    public ResponseEntity removeTask(@AuthenticationPrincipal CustomUser user, @PathVariable final String id) throws Exception {
        taskService.remove(user.getUserId(), id);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/removeAll", method = RequestMethod.DELETE)
    public ResponseEntity removeAllTask(@AuthenticationPrincipal CustomUser user) throws Exception {
        taskService.removeAll(user.getUserId());
        return ResponseEntity.ok().build();
    }
}
